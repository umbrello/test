/***************************************************************************
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   copyright (C) 2012-2020                                               *
 *   Umbrello UML Modeller Authors <umbrello-devel@kde.org>                *
 ***************************************************************************/

#include "layoutgenerator.h"

#include "associationline.h"
#include "associationwidget.h"
#include "cmds.h"
#include "debug_utils.h"
#include "floatingtextwidget.h"
#include "uml.h"
#include "umlwidget.h"

// kde includes
#include <KConfigGroup>
#include <KDesktopFile>
#include <KLocalizedString>
#if QT_VERSION < 0x050000
#include <KStandardDirs>
#endif

// qt includes
#include <QDir>
#include <QFile>
#include <QHash>
#include <QProcess>
#include <QRegExp>
#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#endif
#include <QString>
#include <QTemporaryFile>
//#include <QTextStream>

//#define USE_XDOT

//#define START_PNGVIEWER

#define LAYOUTGENERATOR_DEBUG
//#define LAYOUTGENERATOR_DATA_DEBUG

//#define SHOW_CONTROLPOINTS

#ifdef LAYOUTGENERATOR_DEBUG

static QString pngViewer()
{
#ifdef Q_OS_WIN
    return QLatin1String("start");
#else
#ifdef Q_OS_MAC
    return QLatin1String("unknown");
#else
    return QLatin1String("okular");
#endif
#endif
}

static QString textViewer()
{
#ifdef Q_OS_WIN
    return QLatin1String("start");
#else
#ifdef Q_OS_MAC
    return QLatin1String("unknown");
#else
    return QLatin1String("mcedit");
#endif
#endif
}
#endif

#ifdef SHOW_CONTROLPOINTS
static QGraphicsPathItem *s_debugItems;
static QPainterPath s_path;
#endif

/**
 * constructor
*/
LayoutGenerator::LayoutGenerator()
  : m_nodePositions(m_scale)
{
    setUseFullNodeLabels(false);
}

/**
 * Return state if layout generator is enabled.
 * It is enabled when the dot application has been found.
 *
 * @return true if enabled
*/
bool LayoutGenerator::isEnabled()
{
    return !m_dotPath.isEmpty();
}

/**
 * generate layout and apply it to the given diagram.
 *
 * @return true if generating succeeded
*/
bool LayoutGenerator::generate(UMLScene *scene, const QString &variant)
{
    QTemporaryFile in;
    QTemporaryFile out;
    QTemporaryFile xdotOut;
    if (!isEnabled()) {
        uWarning() << "Could not apply autolayout because graphviz installation has not been found.";
        return false;
    }

#ifdef SHOW_CONTROLPOINTS
    if (!s_debugItems) {
        s_debugItems = new QGraphicsPathItem;
        scene->addItem(s_debugItems);
    }
    s_path = QPainterPath();
    s_debugItems->setPath(s_path);
#endif
#ifdef LAYOUTGENERATOR_DEBUG
    in.setAutoRemove(false);
    out.setAutoRemove(false);
    xdotOut.setAutoRemove(false);
#endif

    // generate filenames
    in.open();
    in.close();
    out.open();
    out.close();
    xdotOut.open();
    xdotOut.close();

#ifdef LAYOUTGENERATOR_DEBUG
    qDebug() << textViewer() << in.fileName();
    qDebug() << textViewer() << out.fileName();
    qDebug() << textViewer() << xdotOut.fileName();
#endif

    if (!createDotFile(scene, in.fileName(), variant))
        return false;

    m_nodePositions.setOrigin(m_origin);
    QString executable = generatorFullPath();

    QProcess p;
    QStringList args;
    args << QLatin1String("-o") << out.fileName() << QLatin1String("-Tplain-ext") << in.fileName();
    p.start(executable, args);
    p.waitForFinished();

    args.clear();
    args << QLatin1String("-o") << xdotOut.fileName() << QLatin1String("-Txdot") << in.fileName();
    p.start(executable, args);
    p.waitForFinished();

#ifdef LAYOUTGENERATOR_DEBUG
    QTemporaryFile pngFile;
    pngFile.setAutoRemove(false);
    pngFile.setFileTemplate(QDir::tempPath() + QLatin1String("/umbrello-layoutgenerator-XXXXXX.png"));
    pngFile.open();
    pngFile.close();
    args.clear();
    args << QLatin1String("-o") << pngFile.fileName() << QLatin1String("-Tpng") << in.fileName();
    p.start(executable, args);
    p.waitForFinished();
    qDebug() << pngViewer() << pngFile.fileName();
#ifdef START_PNGVIEWER
    args.clear();
    args << pngFile.fileName();
    p.startDetached(pngViewer(), args);
#endif
#endif
#ifndef USE_XDOT
    if (!readGeneratedDotFile(out.fileName()))
#else
    if (!readGeneratedDotFile(xdotOut.fileName()))
#endif
        return false;

    return true;
}

/**
 * apply auto layout to the given scene
 * @param scene
 * @return true if autolayout has been applied
 */
bool LayoutGenerator::apply(UMLScene *scene)
{
    foreach(AssociationWidget *assoc, scene->associationList()) {
        AssociationLine *path = assoc->associationLine();
        QString type = Uml::AssociationType::toString(assoc->associationType()).toLower();
        QString key = QLatin1String("type::") + type;

        QString id;
        if (m_edgeParameters.contains(QLatin1String("id::") + key) && m_edgeParameters[QLatin1String("id::") + key] == QLatin1String("swap"))
            id = fixID(Uml::ID::toString(assoc->widgetLocalIDForRole(Uml::RoleType::A)) + Uml::ID::toString(assoc->widgetLocalIDForRole(Uml::RoleType::B)));
        else
            id = fixID(Uml::ID::toString(assoc->widgetLocalIDForRole(Uml::RoleType::B)) + Uml::ID::toString(assoc->widgetLocalIDForRole(Uml::RoleType::A)));

        // adjust associations not used in the dot file
        if (!m_nodePositions.edges().contains(id)) {
            // shorten line path
            AssociationLine *path = assoc->associationLine();
            if (path->count() > 2 && assoc->widgetLocalIDForRole(Uml::RoleType::A) != assoc->widgetLocalIDForRole(Uml::RoleType::B)) {
                while(path->count() > 2)
                    path->removePoint(1);
            }
            continue;
        }

        // set label position
        QPointF &l = m_nodePositions.edgeLabelPositions()[id];
        FloatingTextWidget *tw = assoc->nameWidget();
        if (tw) {
            tw->setPos(mapToScene(l));
        }

        // setup line points
        EdgePoints &p = m_nodePositions.edges()[id];
        int len = p.size();
#ifdef SHOW_CONTROLPOINTS
        QPolygonF pf;
        QFont f;
        for (int i=0; i < len; i++) {
            pf << mapToScene(p[i]);
            s_path.addText(mapToScene(p[i] + QPointF(5,0)), f, QString::number(i));
        }

        s_path.addPolygon(pf);
        s_path.addEllipse(mapToScene(l), 5, 5);
        s_debugItems->setPath(s_path);
#endif
        if (m_version <= 20130928) {
            path->setLayout(Uml::LayoutType::Direct);
            path->cleanup();
            path->setEndPoints(mapToScene(p[0]), mapToScene(p[len-1]));
        } else {
            path->setLayout(Settings::optionState().generalState.layoutType);
            path->cleanup();

            if (Settings::optionState().generalState.layoutType == Uml::LayoutType::Polyline) {
                for (int i = 0; i < len; i++) {
                    if (i > 0 && p[i] == p[i-1])
                        continue;
                    path->addPoint(mapToScene(p[i]));
                }
            } else if(Settings::optionState().generalState.layoutType == Uml::LayoutType::Spline) {
                for (int i = 0; i < len; i++) {
                    path->addPoint(mapToScene(p[i]));
                }
            } else if (Settings::optionState().generalState.layoutType == Uml::LayoutType::Orthogonal) {
                for (int i = 0; i < len; i++) {
                    path->addPoint(mapToScene(p[i]));
                }
            } else
                path->setEndPoints(mapToScene(p[0]), mapToScene(p[len-1]));
        }
    }

    UMLApp::app()->beginMacro(i18n("Apply layout"));

    foreach(UMLWidget *widget, scene->widgetList()) {
        QString id = Uml::ID::toString(widget->localID());
        if (!m_nodePositions.nodes().contains(id))
            continue;
        if (widget->isPortWidget() || widget->isPinWidget())
            continue;

#ifdef SHOW_CONTROLPOINTS
        s_path.addRect(QRectF(mapToScene(m_nodes[id].bottomLeft()), m_nodes[id].size()));
        s_path.addRect(QRectF(origin(id), m_nodes[id].size()));
        s_debugItems->setPath(s_path);
#endif
        QPointF p = origin(id);
        widget->setStartMovePosition(widget->pos());
        widget->setX(p.x());
        widget->setY(p.y()-widget->height());
        widget->adjustAssocs(widget->x(), widget->y());    // adjust assoc lines

        UMLApp::app()->executeCommand(new Uml::CmdMoveWidget(widget));
    }
    UMLApp::app()->endMacro();

    foreach(AssociationWidget *assoc, scene->associationList()) {
        assoc->calculateEndingPoints();
        if (assoc->associationLine())
            assoc->associationLine()->update();
        assoc->resetTextPositions();
        assoc->saveIdealTextPositions();
    }
    return true;
}

/**
 * Return a list of available templates for a given scene type
 *
 * @param scene The diagram
 * @param configFiles will contain the collected list of config files
 * @return true if collecting succeeds
 */
bool LayoutGenerator::availableConfigFiles(UMLScene *scene, QHash<QString,QString> &configFiles)
{
    QString diagramType = Uml::DiagramType::toString(scene->type()).toLower();
#if QT_VERSION >= 0x050000
    const QStringList dirs = QStandardPaths::locateAll(QStandardPaths::GenericDataLocation, QLatin1String("umbrello5/layouts"), QStandardPaths::LocateDirectory);
    QStringList fileNames;
    foreach(const QString& dir, dirs) {
        const QStringList entries = QDir(dir).entryList(QStringList() << QString::fromLatin1("%1*.desktop").arg(diagramType));
        foreach(const QString& file, entries) {
            fileNames.append(dir + QLatin1Char('/') + file);
        }
    }
#else
    KStandardDirs dirs;
    QStringList fileNames = dirs.findAllResources("data", QString::fromLatin1("umbrello/layouts/%1*.desktop").arg(diagramType));
#endif
    foreach(const QString &fileName, fileNames) {
        QFileInfo fi(fileName);
        QString baseName;
        if (fi.baseName().contains(QLatin1String("-")))
            baseName = fi.baseName().remove(diagramType + QLatin1Char('-'));
        else if (fi.baseName() == diagramType)
            baseName = fi.baseName();
        else
            baseName = QLatin1String("default");
        KDesktopFile desktopFile(fileName);
        configFiles[baseName] = desktopFile.readName();
    }
    return true;
}

/**
 * Return the origin of node based on the bottom/left corner
 *
 * @param id The widget id to fetch the origin from
 * @return QPoint instance with the coordinates
 */
QPointF LayoutGenerator::origin(const QString &id)
{
    QString key = fixID(id);
    if (!m_nodePositions.nodes().contains(key)) {
#ifdef LAYOUTGENERATOR_DATA_DEBUG
        uDebug() << key;
#endif
        return QPoint(0,0);
    }
    QRectF &r = m_nodePositions.nodes()[key];
    QPointF p(m_origin.x() + r.x() - r.width()/2, m_nodePositions.boundingRect().height() - r.y() + r.height()/2 + m_origin.y());
#ifdef LAYOUTGENERATOR_DATA_DEBUG
    uDebug() << r << p;
#endif
    return p;
}

/**
 * Read generated dot file and extract positions
 * of the contained widgets.
 *
 * @return true if extracting succeeded
*/
bool LayoutGenerator::readGeneratedDotFile(const QString &fileName)
{
    return m_nodePositions.readFile(fileName);
}

/**
 * map dot coordinate to scene coordinate
 * @param p dot point to map
 * @return uml scene coordinate
 */
QPointF LayoutGenerator::mapToScene(const QPointF &p)
{
    return m_nodePositions.mapToScene(p);
}

#if 0
static QDebug operator<<(QDebug out, LayoutGenerator &c)
{
    out << "LayoutGenerator:"
        << "m_boundingRect:" << c.m_boundingRect
        << "m_executable:" << c.m_executable;
    return out;
}
#endif

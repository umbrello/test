/*
    Copyright 2020 Ralf Habacker  <ralf.habacker@freenet.de>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "testsequenceimport.h"

#include "diagram_utils.h"

//-----------------------------------------------------------------------------

class TestData
{
public:
    static QList<TestData> s_tests;
    QString m_file;
    QString m_module;
    QString m_in;
    QString m_package;
    QString m_method;

    TestData(const char *in, const char *package, const char *method)
      : m_in(in),
        m_package(package),
        m_method(method)
    {
        s_tests.append(*this);
    }

    TestData(const char *file, const char *module, const char *in, const char *package, const char *method)
      : m_file(file),
        m_module(module),
        m_in(in),
        m_package(package),
        m_method(method)
    {
        s_tests.append(*this);
    }

    void check() const
    {
        QString package;
        QString method;

        QVERIFY(Diagram_Utils::parseIdentifier(m_file, m_module, m_in, package, method));
        QCOMPARE(package, m_package);
        QCOMPARE(method, m_method);
    }
};

QList<TestData> TestData::s_tests;

void TestSequenceImport::test_parsedotIdentifier()
{
    // common
    TestData("file.cpp", "", "main", "file.cpp", "main()");
    TestData("", "module", "main", "module", "main()");

    // generated by gprof2dot.py
    TestData("test-profil\\nBasicCondition::BasicCondition(BasicCondition\\nconst&)\\n0.00%\\n(0.00%)\\n3×",
             "BasicCondition",
             "BasicCondition(BasicCondition const&)");
    TestData("test-profil\\nBasicCondition::~BasicCondition()\\n0.00%\\n(0.00%)\\n9×",
             "BasicCondition",
             "~BasicCondition()");
    TestData("test-profil\\nabs(Point const&)\\n0.11%\\n(0.09%)\\n288×",
             "test-profil",
             "abs(Point const&)");
    TestData("test-profil\\nCatenaryCurve const&\\nstd::forward<CatenaryCurve\\nconst&>(std::remove_reference<CatenaryCurve\\nconst&>::type&)\\n0.00%\\n(0.00%)\\n4×",
             "std",
             "CatenaryCurve const& forward<CatenaryCurve const&>(std::remove_reference<CatenaryCurve const&>::type&)");
    TestData("test-profil\\nvoid\\nstd::_Destroy_aux<true>::__destroy<CatenaryCurve**>(CatenaryCurve**,\\nCatenaryCurve**)\\n0.00%\\n(0.00%)\\n2×",
             "std::_Destroy_aux<true>",
             "void __destroy<CatenaryCurve**>(CatenaryCurve**, CatenaryCurve**)");
    TestData("test-profil\\nvoid\\nstd::allocator_traits<std::allocator<std::_Rb_tree_node<std::pair<ComputationResults::HashKey\\nconst,ComputationResult*>>>\\n>::construct<std::pair<ComputationResults::HashKey\\nconst,ComputationResult*>,\\nstd::piecewise_construct_t\\nconst&,\\nstd::tuple<ComputationResults::HashKey\\nconst&>,std::tuple<>\\n>(std::allocator<std::_Rb_tree_node<std::pair<ComputationResults::HashKey\\nconst,ComputationResult*>>>&,\\nstd::,air<ComputationResults::HashKey\\nconst,ComputationResult*>*,\\nstd::piecewise_construct_t\\nconst&,\\nstd::tuple<ComputationResults::HashKey\\nconst&>&&,std::tuple<>&&)\\n0.00%\\n(0.00%)\\n1×",
             "std::allocator_traits<std::allocator<std::_Rb_tree_node<std::pair<ComputationResults::HashKey const,ComputationResult*>>> >",
             "void construct<std::pair<ComputationResults::HashKey const,ComputationResult*>, std::piecewise_construct_t const&, std::tuple<ComputationResults::HashKey const&>,std::tuple<> >(std::allocator<std::_Rb_tree_node<std::pair<ComputationResults::HashKey const,ComputationResult*>>>&, std::,air<ComputationResults::HashKey const,ComputationResult*>*, std::piecewise_construct_t const&, std::tuple<ComputationResults::HashKey const&>&&,std::tuple<>&&)");
    TestData("test-profil\\nCatenaryCurve*\\nstd::__uninitialized_copy<false>::__uninit_copy<__gnu_cxx::__normal_iterator<CatenaryCurve\\nconst*,\\nstd::vector<CatenaryCurve,\\nstd::allocator<CatenaryCurve>>\\n>,\\nCatenaryCurve*>(__gnu_cxx::__normal_iterator<CatenaryCurve\\nconst*,\\nstd::vector<CatenaryCurve,\\nstd::allocator<CatenaryCurve>>\\n>,\\n__gnu_cxx::__normal_iterator<CatenaryCurve\\nconst*,\\nstd::vector<CatenaryCurve,\\nstd::allocator<CatenaryCurve>>\\n>,CatenaryCurve*)\\n0.03%\\n(0.00%)\\n2×",
             "std::__uninitialized_copy<false>",
             "CatenaryCurve* __uninit_copy<__gnu_cxx::__normal_iterator<CatenaryCurve const*, std::vector<CatenaryCurve, std::allocator<CatenaryCurve>> >, CatenaryCurve*>(__gnu_cxx::__normal_iterator<CatenaryCurve const*, std::vector<CatenaryCurve, std::allocator<CatenaryCurve>> >, __gnu_cxx::__normal_iterator<CatenaryCurve const*, std::vector<CatenaryCurve, std::allocator<CatenaryCurve>> >,CatenaryCurve*)");

    // kcachegrind
    TestData("main", "", "main()");
    TestData("Options::check(char const*) const", "Options", "check(char const*) const");

    // gdb
    TestData("Import_Utils::importStackTrace(QString const&, UMLScene*)",
             "Import_Utils",
             "importStackTrace(QString const&, UMLScene*)");
    TestData("",
             "/usr/lib64/libglib-2.0.so.0",
             "g_main_context_iteration ()",
             "/usr/lib64/libglib-2.0.so.0",
             "g_main_context_iteration()");
    TestData("/home/xxx/src/umbrello/umbrello/codeimport/import_utils.cpp:715",
             "",
             "Import_Utils::importStackTrace",
             "Import_Utils",
             "importStackTrace()");

    // qtcreator/gdb
    TestData("driver.cpp",
             "",
             "Driver::ParseHelper::ParseHelper",
             "Driver::ParseHelper",
             "ParseHelper()");
    TestData("",
             "/usr/lib64/libglib-2.0.so.0",
             "g_main_context_dispatch",
             "/usr/lib64/libglib-2.0.so.0",
             "g_main_context_dispatch()");

    foreach(const TestData &test, TestData::s_tests) {
        test.check();
    }
}

QTEST_MAIN(TestSequenceImport)
